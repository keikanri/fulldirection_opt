var browserify      = require('browserify');
var exec            = require('child_process').exec;
var gulp            = require('gulp');
var concat          = require('gulp-concat');
var pleeease        = require('gulp-pleeease');
var plumber         = require('gulp-plumber');
var sass            = require('gulp-sass');
var sourcemaps      = require('gulp-sourcemaps');
var uglify          = require('gulp-uglify');
var babel           = require('gulp-babel');
var jquery          = require('jquery');
var source          = require('vinyl-source-stream');
var buffer          = require('vinyl-buffer');
var watchify        = require('watchify');
var handleErrors    = require('./src/lib/handleErrors.js');

gulp.task('sass', function() {
    gulp.src('./src/scss/*.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({
            style : 'compact',
        }))
        .pipe(pleeease({
            autoprefixer: {
                browsers: ['last 2 versions']
            },
            minifier: true
        }))
        .pipe(sourcemaps.write('./', {sourceRoot: './src/scss'}))
        .pipe(gulp.dest('./webroot/css'));
});

gulp.task("watch", [ 'watchify' ], function() {
    gulp.watch('./src/scss/*.scss', ['sass']);
});

/**
 * browserify
 */
gulp.task( 'browserify', function() {
    return jscompile( false );
} );
 
/**
 * watchify
 */
gulp.task( 'watchify', function() {
    return jscompile( true );
} );

function jscompile( is_watch ) {
    var bundler = browserify({
        // Required watchify args
        cache: {}, packageCache: {}, fullPaths: true,
        // Browserify Options
        entries: ['./src/js/app.js'],
    });
    if ( is_watch ) {
        bundler = watchify(bundler);
        bundler.on('update', function() {
            rebundle();
        });
    }
    bundler.on( 'log', function( message ) {
        console.log( message );
    } );

    function rebundle() {
        return bundler
            .bundle()
            .on('error', handleErrors)
            .pipe(source('bundle.js'))
            .pipe(buffer())
            .pipe(babel({compact: true}))
            .pipe(gulp.dest('./webroot/js/'))
    }
    return rebundle();
};

var defaultTasks = [
    'sass',
    'watchify',
    'watch',
];

gulp.task('default', defaultTasks);