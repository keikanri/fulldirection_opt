'use strict';
/**
 * Copyright (c) 2014, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

var $ = require('jquery');
window.$ = $;
global.jQuery = require('jquery');
var moment = require('moment');

moment.locale('ja', {
    weekdays: ["日曜日","月曜日","火曜日","水曜日","木曜日","金曜日","土曜日"],
    weekdaysShort: ["日","月","火","水","木","金","土"],
});

var courseStartDate = moment().add(2, 'day').format('YYYY年M月D日（ddd）19:00~21:00');
var deadlineDate = moment().add(2, 'day').format('M月D日19時');
var subscribeFormValue = moment().add(2, 'day').format('YYYY-MM-DD 19:00:00');

var now = new Date();
var xday = new Date(2016, 10-1, 4); /* 0-11なので0が1月*/
var countDate = Math.ceil( (xday.getTime() - now.getTime()) / (24*60*60*1000) );

$(function() {
    $('.subscribe__dateTime').html(courseStartDate);
    $('.appointmentDate').val(subscribeFormValue);
    $('.deadlineDate').html(deadlineDate);
    $('.countDate').html(countDate);
});